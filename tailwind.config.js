module.exports = {
  purge: {
    enabled: false,
    content: ['src/**/*.html', 'src/**/*.njk', 'src/**/*.svg'],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundSize: {
        'size-diamond2': '95%',
        'size-md': '768px',
        'size-lg': '1024px',
        'size-xl': '1280px',
        'size-2xl': '1920px'
      },
      colors: {
        purple: {
          800: '#3c2f48',
          900: '#2e2634'
        }
      },
      height: {
        '16': '4.5rem',
        '130': '35rem',
        '192': '48rem',
        '240': '60rem',
        '288': '72rem',
        '320': '80rem',
        '360': '90rem'
      },
      margin: {
        '-240': '-60rem',
        '-200': '-50rem',
        '-160': '-40rem',
        '-128': '-32rem',
        '-112': '-28rem',
        '-88': '-22rem'
      },
      maxWidth: {
        '4xl': '60rem'
      },
      minWidth: {
        '16': '4rem',
        '40': '10rem',
        '48': '12rem',
        '52': '13rem',
        '56': '14rem',
        '60': '15rem',
        '68': '17rem',
        '80': '20rem'
      },
      padding: {
        '19': '4.75rem'
      },
      width: {
        '18': '4.5rem',
        '27': '6.75rem'
      },
      zIndex: {
        '-10': '-10'
      }
    }
  },
  variants: {
    extend: {
      margin: ['focus', 'hover']
    }
  },
  plugins: [],
}
